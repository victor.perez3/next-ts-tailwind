module.exports = {
    purge: ["./components/**/*.js", "./pages/**/*.js"],
    theme: {
      extend: {
        colors: {
          primary: "#00A75D",
          accent: "",
          _1: "#1D293F",
          _2: "#20E9BC",
          _3: "#F2C94C",
          _4: "#FF374F",
        },
        fontFamily: {
          rubik: "Rubik, serif",
        },
        lineHeight: {
          12: "3rem",
        },
        transitionProperty: {
          height: "height",
          width: "width",
        },
        transitionDuration: {
          "2500": "2500ms",
        },
      },
    },
    variants: {},
    plugins: [require("@tailwindcss/forms")],
  };