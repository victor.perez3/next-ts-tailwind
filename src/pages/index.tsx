import { useState } from 'react';
import { Input, Form, Breadcrumb, SidePanel } from '@/versedui'
import { Company } from '@/types';
export default function Home() {
  const [open, setOpen] = useState(false);

  const links =[
    { name: 'Admin', url: '/admin', active: false },
    { name: 'Evidences', url: '/evidences', active: true }
  ]

  const submit = (values) => {
    console.log(values)
    setCompanies([...companies, values])
    setOpen(!open)
  }

  const [companies, setCompanies] = useState([
    {
      name: 'Jane Cooper',
      alias: 'Regional Paradigm Technician'
    }
  ])

  return (
    <article>
      <div className="space-y-2">
        <h1 className="text-6xl font-semibold">Admin Panel</h1>
        <h2 className="text-2xl font-light text-gray-500">Welcome to the Versed AI Admin Panel</h2>
        <hr />
      </div>
      <div className="mx-4 my-2">
        <Breadcrumb links={links} />
        <button onClick={() => setOpen(!open)}>Open</button>
        <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
          <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Name
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Title
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Status
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Role
                  </th>
                  <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {companies.map((company) => (
                  <tr key={company.name}>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="flex items-center">
                        <div className="flex-shrink-0 h-10 w-10">
                          <img className="h-10 w-10 rounded-full" src={company.name} alt="" />
                        </div>
                        <div className="ml-4">
                          <div className="text-sm font-medium text-gray-900">{company.name}</div>
                          <div className="text-sm text-gray-500">{company.name}</div>
                        </div>
                      </div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <div className="text-sm text-gray-900">{company.name}</div>
                      <div className="text-sm text-gray-500">{company.alias}</div>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap">
                      <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        Active
                      </span>
                    </td>
                    <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">{company.name}</td>
                    <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                      <a href="#" className="text-indigo-600 hover:text-indigo-900">
                        Edit
                      </a>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
        <div className="my-4">
          <SidePanel isOpen={open} close={() => setOpen(!open)} >
          <article className="p-4 pr-8 space-y-6">
          <section>
            <header>
              <h3 className="text-2xl font-medium text-gray-900 leading-6">Create a new user</h3>
              <p className="mt-2 text-sm text-gray-500">Set up a new user for this PyGrid Domain.</p>
            </header>
          </section>
            <Form onSubmit={submit} defaultValues={undefined}>
              <Input 
              name="name"
              label="Name" 
              placeholder="Write something!!!" 
              isValid={true}
              type="text"
              size="lg" />
              <Input 
              name="alias"
              label="Password" 
              placeholder="Write something!" 
              isValid={true}
              type="password" 
              size="md"/>
            </Form>
            </article>
          </SidePanel>
        </div>
      </div>
    </article>
  )
}