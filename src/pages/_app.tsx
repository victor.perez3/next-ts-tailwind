import type { ReactNode, FunctionComponent } from "react";
import React from "react";
import type { AppProps } from "next/app";
import { ReactQueryDevtools } from "react-query/devtools";
import { getLayout as getAppLayout } from "@/layouts/app";

import "@/styles/globals.css";

type AppPropsWithLayout = {
  Component: { getLayout: (FunctionComponent) => ReactNode };
} & AppProps;

export default function VictorBlog({
  Component,
  pageProps,
}: AppPropsWithLayout) {
  const getLayout = Component.getLayout || getAppLayout;
  return (
    <>
      {getLayout(<Component {...pageProps} />)}
      {process.env.ENVIRONMENT === "development" && (
        <ReactQueryDevtools initialIsOpen={false} />
      )}
    </>
  );
}