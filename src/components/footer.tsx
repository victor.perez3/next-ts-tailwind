import type { FunctionComponent } from "react";
import React from "react";

const Footer: FunctionComponent = () => {
  return (
    <footer>
      <nav className="sticky bottom-0 py-8 border bt-1 bg-gray-100">
        <div className="flex items-center justify-center">
          All Rights Reserved
        </div>
      </nav>
    </footer>
  );
};

export { Footer };