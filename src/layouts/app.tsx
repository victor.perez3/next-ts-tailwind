import type { FunctionComponent } from "react";
import React from "react";
import { Footer } from "@/components/footer";
import { Header } from "@/components/header";

export const Layout: FunctionComponent = ({ children }) => (
  <div className="flex flex-col h-screen">
    <Header />
    <main className="container mx-auto mt-8 mb-12 flex-grow">{children}</main>
    <Footer />
  </div>
);

export const getLayout = (page) => <Layout>{page}</Layout>;