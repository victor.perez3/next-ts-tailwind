import React, { FunctionComponent, useState, useEffect } from "react";
import { Transition } from "@headlessui/react";
import { XIcon, CheckCircleIcon, ExclamationIcon, ExclamationCircleIcon } from '@heroicons/react/solid'
import cn from 'classnames'

interface NotificationProps {
    title: string
    text: string
    type: 'success' | 'warning' | 'error'
}

const Notification : FunctionComponent<NotificationProps>= ({title, text, type}) => {
    const [isShowing, show] = useState(true)

    useEffect(() => {
      const timer = setTimeout(() => show(false), 5000)
      return () => clearTimeout(timer)
    }, [])

    return (
        <Transition show={isShowing}>
            <div className="fixed inset-0 flex items-end justify-center px-4 py-6 pointer-events-none sm:p-6 sm:items-start sm:justify-end">
                <Transition.Child
                    enter="transform ease-in-out duration-300 transition"
                    enterFrom="translate-y-96 opacity-0 sm:translate-y-0 sm:translate-x-96"
                    enterTo="translate-y-0 opacity-100 sm:translate-x-0"
                    leave="transition ease-in duration-100"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0">
                    <div className="w-screen max-w-sm overflow-hidden bg-white rounded-lg shadow-lg pointer-events-auto ring-1 ring-black ring-opacity-5">
                        <div className="p-4">
                            <div className="flex">
                                <div className="flex items-center justify-center">
                                    {
                                        type == 'success' ?
                                        <CheckCircleIcon className="w-10 text-green-600" /> :
                                        ( type == 'warning' ? 
                                        <ExclamationIcon className="w-10 text-yellow-400" /> :
                                        <ExclamationCircleIcon className="w-10 text-red-600" />
                                        )
                                    }
                                </div>
                                <div className="ml-3 w-0 flex-1 pt-0.5">
                                    <p className={cn("text-base font-medium text-gray-900", type == 'success' ? 'text-green-600' : ( type == 'warning' ? 'text-yellow-400' : 'text-red-600'))}>{title ?? 'Success!'}</p>
                                    <p className="mt-1 text-sm text-gray-500">{text}</p>
                                </div>
                                <div className="flex flex-shrink-0 ml-4">
                                    <button
                                    className="inline-flex text-gray-400 bg-white rounded-md hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                    onClick={() => show(false)}>
                                    <span className="sr-only">Close</span>
                                    <XIcon className="w-5 h-5" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Transition.Child>
            </div>
      </Transition>
    )
}

export {
    Notification
}