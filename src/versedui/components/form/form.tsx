import React, { FunctionComponent } from "react";
import { useForm } from "react-hook-form";
import { Notification } from "@/versedui";

const Form: FunctionComponent<any> = ({ defaultValues, children, onSubmit }) => {
  const methods = useForm({ defaultValues });
  const { formState, handleSubmit } = methods;

  return (
    <>
    <form className="space-y-6" onSubmit={handleSubmit(onSubmit)}>
      {React.Children.map(children, child => {
        return child.props.name
          ? React.createElement(child.type, {
              ...{
                ...child.props,
                register: methods.register,
                errors: methods.formState.errors,
                key: child.props.name
              }
            })
          : child;
       })}
       <button className="bg-indigo-600 py-2 px-4 border rounded-lg text-white font-semibold">
           Submit
       </button>
    </form>
    {formState.isSubmitSuccessful ?
    <Notification title="User Created" text="The user has been created successfully!" type="success" /> : null}
    </>
  );
}

export {
    Form
}