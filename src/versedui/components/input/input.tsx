import React, { FunctionComponent, HTMLProps, HTMLInputTypeAttribute } from "react"
import cn from 'classnames';
import { ExclamationIcon } from '@heroicons/react/solid'
interface InputProps {
    register?: any
    name: string
    label: string
    className?: string
    isValid: boolean
    errors?: Object
    size: any
}

const Input: FunctionComponent<InputProps & HTMLProps<HTMLInputTypeAttribute>> = ({ register, label, name, isValid, errors, size, ...props }) => {
    const s = size == "sm" ? "input-md" : "input-lg"
    return (
        <div className="space-y-1">
            <section className="relative">
                <label for={name} className="text-base">{label}</label>
                <input id={name} {...props} {...register(name, { required: 'This is an error'})} className={cn(!errors[name] ? "input" : "input input-invalid", "w-full flex-1", s)} />
                {errors[name] ? <ExclamationIcon className="w-6 h-6 text-red-500 absolute right-2 bottom-2" /> : null }
            </section>
            <small className={cn(errors[name] ? "" : "", "text-red-500 text-sm flex")}>{errors[name]?.message}</small>
        </div> 

    )
}

interface InputTitleProps {
    title: string;
}

const InputLabel: FunctionComponent<InputTitleProps> = ({title}) => {
    return (
        <p className="text-xl">{title}</p>
    )
}

export {
    Input,
    InputLabel
}