import { FunctionComponent } from "react";
import cn from "classnames";
import {ChevronRightIcon} from "@heroicons/react/solid"

interface CrumbProps {
    name: string
    url: string
    active: boolean
}

interface BreadcrumbProps {
    links : CrumbProps[]
}

const Breadcrumb : FunctionComponent<BreadcrumbProps> = ({links}) => {
    return (
        <div className="flex">
            {links.map(link => {
                return(
                    <div className="flex items-center justify-center">
                        <a className={cn(link.active ? "text-gray-700" : "text-gray-400", "flex hover:underline hover:text-indigo-600 font-semibold")} href={link.url}>
                            {link.name}
                        </a>
                        <div className="pr-4 pl-4 text-gray-400 flex">
                            <ChevronRightIcon className="w-6 h-6" />
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export {
    Breadcrumb
}